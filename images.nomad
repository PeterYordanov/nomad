job "images" {
  datacenters = ["dc1"]
  group "images" {
    count = 1
    task "client" {
      driver = "docker"
      config {
        image = "shekeriev/dob-w3-php"
        args  = [
          "-listen", ":8081",
        ]
      }
      resources {
        network {
          mbits = 10
          port "http" {
            static = 8081
          }
        }
      }
    }
    task "server" {
      driver = "docker"
      config {
        image = "shekeriev/dob-w3-mysql"
        args  = [
          "-listen", ":8082",
        ]
      }
      resources {
        network {
          mbits = 10
          port "http" {
            static = 8082
          }
        }
      }
    }
  }
}